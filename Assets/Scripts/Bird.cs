﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

    private bool isDead = false;
    private Rigidbody2D rB2D;

    public float upForce = 200;

	// Use this for initialization
	void Start () {
        rB2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(isDead == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rB2D.velocity = Vector2.zero;
                rB2D.AddForce(new Vector2(0, upForce));
            }
        }
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        isDead = true;
    }
}
